/// My first class in Atelier.
Class MyFirstPackage.Hello Extends %Persistent
{

/// Class method
ClassMethod SayHello(args) As %Status
{
	#dim tSC As %Status
	
	Set tSC = $$$OK
	
	Write !,!,! 
	Write "Hello World!", !
	Write "Hello ", args, "!", !
	
	return tSC
}

Storage Default
{
<Data name="HelloDefaultData">
<Value name="1">
<Value>%%CLASSNAME</Value>
</Value>
</Data>
<DataLocation>^MyFirstPackage.HelloD</DataLocation>
<DefaultData>HelloDefaultData</DefaultData>
<IdLocation>^MyFirstPackage.HelloD</IdLocation>
<IndexLocation>^MyFirstPackage.HelloI</IndexLocation>
<StreamLocation>^MyFirstPackage.HelloS</StreamLocation>
<Type>%Library.CacheStorage</Type>
}

}
